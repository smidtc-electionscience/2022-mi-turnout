The 2022 Michigan Electorate: An Analysis (and Prediction) from the
State Voterfile
================
2022 November 7

  - [Headline numbers](#headline-numbers)
  - [What can we learn about Michigan’s
    electorate?](#what-can-we-learn-about-michigans-electorate)
      - [The continued purpling of Michigan’s
        electorate](#the-continued-purpling-of-michigans-electorate)
      - [The Turnout Gap Among Congressional
        Districts](#the-turnout-gap-among-congressional-districts)
      - [Michigan voters will be older, but the youth vote is showing
        staying
        power](#michigan-voters-will-be-older-but-the-youth-vote-is-showing-staying-power)
      - [Expected County Turnout](#expected-county-turnout)
  - [Context and background: the Michigan Qualified Voter File (QVF) and
    voter registration
    laws](#context-and-background-the-michigan-qualified-voter-file-qvf-and-voter-registration-laws)
  - [Prediction track record](#prediction-track-record)
      - [How these predictions are
        made](#how-these-predictions-are-made)
      - [Model specifics](#model-specifics)
      - [Subsampling and prediction
        generation](#subsampling-and-prediction-generation)

Corwin D. Smidt (<smidtc@msu.edu>)

# Headline numbers

  - **Voterfile-based predicted turnout rate among registered voters**:
    Depending on assumptions, ranges in between **53-54%**.

  - **Expected number of voters**: Depending on assumptions, ranges in
    between **4.35-4.43 million voters**.

  - **Adding uncertainty**, it has been hard to measure uncertainty with
    changes in the law. The estimate has been off 1-3 percentage points
    in past years. So that suggests a reasonable range of outcomes is
    50-57 percentage points or 4.1-4.7 million.

So turnout rates look very much likely to be lower than 2018 (58%), and
higher than other recent midterms (such as 2014 and 2010). A comparable
election would be 2006 (53%). But, considering the larger number of
registered voters, we would still expect over 4 million votes. This
expectation is consistent with the pattern we saw in August 2022. Not as
many people voted in 2022 as in 2020, but it is still one of only three
August elections that saw over 2 million people vote.

# What can we learn about Michigan’s electorate?

## The continued purpling of Michigan’s electorate

Just based on registration patterns alone Michigan is becoming more a
more closely divided state.

Despite voterfile-based expectations of relatively high midterm turnout
(which helped them in 2006), the balance of voters across the state
continues to shift away from Democratic strongholds. [Political
scientists find little evidence that higher turnout benefits Democrats
over
Republicans](https://www.amazon.com/Turnout-Myth-Partisan-Outcomes-Elections/dp/0190089458).
This would hold for 2022 and Michigan.

**We continue to see consistent declines in the relative number of
registered voters in Democratic counties.** This includes in Wayne
County, and new signs of decline in Oakland. In contrast, we see
continued growth in Macomb, Kent, and Ottawa.

So even if everyone votes for the same party as they have over the last
ten years, we would expect an improvement among Republicans. To show
this, I set each county’s expected vote to be equal to the average
presidential vote margin (Democrat %-Republican %)

![](graph-1.png)<!-- -->

## The Turnout Gap Among Congressional Districts

Further evidence of the GOP advantage in turnout can be seen by
comparing turnout expectations in Congressional districts. These were
redrawn last year to be nearly equal in population. But a comparison of
turnout rates shows there is a larger difference in the size of each
district’s electorate, most notably the two Detroit districts (12 and
13).

| District | Party Lean | Exp. \# of Voters |
| -------: | :--------- | ----------------: |
|        1 | Rep        |            388481 |
|        2 | Rep        |            337486 |
|        3 | Even       |            328768 |
|        4 | Rep        |            331418 |
|        5 | Rep        |            315404 |
|        6 | Dem        |            373517 |
|        7 | Even       |            355469 |
|        8 | Even       |            338296 |
|        9 | Rep        |            369400 |
|       10 | Rep        |            334764 |
|       11 | Dem        |            370109 |
|       12 | Dem        |            303275 |
|       13 | Dem        |            257529 |

2022 Turnout by Congressional District

## Michigan voters will be older, but the youth vote is showing staying power

Compared to 2018, the expectation is that this midterm electorate will
be older; the median age among voters is likely 57. The median age in
2018 was 55, which was much closer to the lower ages we see in
presidential elections (53 in 2016 and 52 in 2020).

Although this change in age favors Republicans, the trends show it isn’t
all good news for Republicans. Compare the relative size of voting
groups under 60 years old.

| Age Group  | 2014 | 2016 | 2018 | 2020 | 2022   |
| ---------- | ---- | ---- | ---- | ---- | ------ |
|            |      |      |      |      |        |
| **18-29**  | 6.0  | 13.6 | 11.7 | 15.5 | \~11.9 |
|            |      |      |      |      |        |
| **30-44**  | 16.8 | 21.3 | 20.2 | 21.9 | \~18.6 |
|            |      |      |      |      |        |
| **45-59**  | 30.6 | 29.1 | 27.8 | 26.0 | \~25.2 |
|            |      |      |      |      |        |
| **60+**    | 46.6 | 36.1 | 40.3 | 36.6 | \~44.3 |
|            |      |      |      |      |        |
| **Median** | 58   | 53   | 55   | 52   | \~57   |
|            |      |      |      |      |        |

The size of the youth vote is expected to increase over 2018. That is
the most pro-Democratic group in the state. In comparison advanced
middle age voters (45-59) continue to decline in relative size. In
Michigan polls, this Generation X group of 45-59 year-olds leans the
most towards the GOP. This change in relative size is largely driven by
the increased number of registrations among these younger groups. For
example, a breakdown of registration numbers and turnout rates show,
that the 45-59 group is now a smaller portion of registered voters in
Michigan than the 30-44 year old group. If these voters stay in
Michigan, we should expect this group of younger voters to become much
more influential as the baby boomer generation declines in Michigan.

| Age Group | Millions Registered | Exp. Turnout Rate | Millions Voting |
| --------- | ------------------- | ----------------- | --------------- |
|           |                     |                   |                 |
| **18-29** | 1.44                | 35%               | 0.53            |
|           |                     |                   |                 |
| **30-44** | 2.03                | 41%               | 0.82            |
|           |                     |                   |                 |
| **45-59** | 1.97                | 56%               | 1.11            |
|           |                     |                   |                 |
| **60+**   | 2.74                | 72%               | 1.95            |
|           |                     |                   |                 |

## Expected County Turnout

| Number | County       | Exp. \# of Voters |
| -----: | :----------- | ----------------: |
|      1 | ALCONA       |              5819 |
|      2 | ALGER        |              4578 |
|      3 | ALLEGAN      |             54577 |
|      4 | ALPENA       |             14186 |
|      5 | ANTRIM       |             14206 |
|      6 | ARENAC       |              7294 |
|      7 | BARAGA       |              2983 |
|      8 | BARRY        |             30677 |
|      9 | BAY          |             48272 |
|     10 | BENZIE       |             10797 |
|     11 | BERRIEN      |             64104 |
|     12 | BRANCH       |             17129 |
|     13 | CALHOUN      |             50963 |
|     14 | CASS         |             21495 |
|     15 | CHARLEVOIX   |             14645 |
|     16 | CHEBOYGAN    |             12550 |
|     17 | CHIPPEWA     |             14053 |
|     18 | CLARE        |             13099 |
|     19 | CLINTON      |             39006 |
|     20 | CRAWFORD     |              6852 |
|     21 | DELTA        |             17106 |
|     22 | DICKINSON    |             10977 |
|     23 | EATON        |             50178 |
|     24 | EMMET        |             19755 |
|     25 | GENESEE      |            171797 |
|     26 | GLADWIN      |             11721 |
|     27 | GOGEBIC      |              6693 |
|     28 | GD. TRAVERSE |             51066 |
|     29 | GRATIOT      |             15894 |
|     30 | HILLSDALE    |             18618 |
|     31 | HOUGHTON     |             14553 |
|     32 | HURON        |             14734 |
|     33 | INGHAM       |            113329 |
|     34 | IONIA        |             25763 |
|     35 | IOSCO        |             13182 |
|     36 | IRON         |              5962 |
|     37 | ISABELLA     |             21871 |
|     38 | JACKSON      |             63400 |
|     39 | KALAMAZOO    |            112315 |
|     40 | KALKASKA     |              8922 |
|     41 | KENT         |            287448 |
|     42 | KEWEENAW     |              1371 |
|     43 | LAKE         |              5408 |
|     44 | LAPEER       |             42948 |
|     45 | LEELANAU     |             15007 |
|     46 | LENAWEE      |             39664 |
|     47 | LIVINGSTON   |            103821 |
|     48 | LUCE         |              3054 |
|     49 | MACKINAC     |              6159 |
|     50 | MACOMB       |            385038 |
|     51 | MANISTEE     |             12407 |
|     52 | MARQUETTE    |             30246 |
|     53 | MASON        |             15010 |
|     54 | MECOSTA      |             16901 |
|     55 | MENOMINEE    |              9683 |
|     56 | MIDLAND      |             41136 |
|     57 | MISSAUKEE    |              7038 |
|     58 | MONROE       |             65483 |
|     59 | MONTCALM     |             27236 |
|     60 | MONTMORENCY  |              5378 |
|     61 | MUSKEGON     |             71618 |
|     62 | NEWAYGO      |             22219 |
|     63 | OAKLAND      |            624368 |
|     64 | OCEANA       |             12002 |
|     65 | OGEMAW       |             10400 |
|     66 | ONTONAGON    |              3284 |
|     67 | OSCEOLA      |              9853 |
|     68 | OSCODA       |              4259 |
|     69 | OTSEGO       |             11872 |
|     70 | OTTAWA       |            138362 |
|     71 | PRESQUE ISLE |              7083 |
|     72 | ROSCOMMON    |             12936 |
|     73 | SAGINAW      |             81917 |
|     74 | ST. CLAIR    |             17079 |
|     75 | ST. JOSEPH   |              3281 |
|     76 | SANILAC      |             32435 |
|     77 | SCHOOLCRAFT  |             71878 |
|     78 | SHIAWASSEE   |             27324 |
|     79 | TUSCOLA      |             21753 |
|     80 | VAN BUREN    |             31598 |
|     81 | WASHTENAW    |            179199 |
|     82 | WAYNE        |            678457 |
|     83 | WEXFORD      |             15123 |

2022 Turnout by County

# Context and background: the Michigan Qualified Voter File (QVF) and voter registration laws

The QVF is a state records of registered voters and their recent turnout
history. The Secretary of State is [mandated by
law](http://legislature.mi.gov/\(S\(sqamwbpcof0jluayrwguteex\)\)/mileg.aspx?page=getobject&objectname=mcl-168-509q)
to keep a centralized, active list of qualified electors in Michigan
along with their recent voting turnout histories.

But the data set is not historic record. Michigan registration records
suffer inaccuracies because of our decentralized nature and mismatched
laws. The Secretary of State relies on a network of 1500+ local clerks
to inform the state of changes in registration status and voter turnout
records. The State shares the responsibility of registering voters and
administering the QVF because the state allows voter registration when
applying for a drivers license, but local clerks also register voters
and are responsible for keeping voter turnout records and reporting
these records to the state.

And the recent constitutional amendment passed by voters, 2018’s
Proposal 3, made it easier for residents to register (and vote), even if
they live for a short time in Michigan. But there was no consideration
of its impact on state registration requirements and the laws for
removing past registrants.

For example, the state currently reports we have a total of [8.2
million](https://mvic.sos.state.mi.us/VoterCount/Index) registered
voters. This is 10% larger than registered voters in 2018 despite little
population growth. And **this total is larger than scholarly estimates
of Michigan’s Census voting-eligible population**, which is [7.9
million](https://www.electproject.org/2022g).

A primary reason for this mismatch is that [state law requires a lengthy
process for the identification and cancellation of voter
registrations](https://www.michigan.gov/sos/~/link.aspx?_id=0CA77C36E2D44E0DBCAB875DE164507F&_z=z).
The recent state QVF I received this month, currently lists the total
number of voters slated for cancellation at over 670,000 (8%).

The Fall 2020 QVF listed around 460,000 (6%) as slated for cancellation.
And about 11-12% of those slated voters return to active status by
voting in subsequent elections. In 2018 (before Proposal 3 passed), the
rate of voters slated for cancellation in the QVF was about 5%.

Interestingly, [northern counties are the most
over-registered](https://twitter.com/CorySmidt/status/1551662559333433344/photo/1),
perhaps reflecting the impact of the pandemic on choice of residency
during the fall of 2020. And counties with large universities (e.g.,
Ingham, Issabella, Mecosta, Washtenaw) have the largest proportion of
voters slated for cancellation, reflecting the transient status of
college students in these areas.

# Prediction track record

  - In 2018, the same model and approach suggested record high and
    accurate predictions before the [November 2018
    midterm](https://gitlab.com/smidtc-electionscience/2018predictions/-/blob/169f95480795a3a574230aedd98c6eb11401deb7/2018predictions.pdf).
  - In 2020, the same model did well predicting the [presidential
    primary in
    March](https://twitter.com/CorySmidt/status/1237721790350155777),
    again with a slight underestimate. But it [over-estimated turnout in
    November
    of 2020](https://smidtc.gitlab.io/blog/post/2020-10-20_nov20turnout/).
    Post-election analysis indicated about half of this over-estimate
    was driven by the failure to discount the large increase of inactive
    voter (see below).

## How these predictions are made

Model estimates of voter turnout are generated from looking at the
turnout history registered voter in Michigan as listed in the state’s
database September. It estimates how age, address, and past voting
behavior typically predicts voter turnout. And then predicts an
individual-level (and jurisdiction-level) estimate of the probability of
voting for 2022, and then it simulates an election across these
observations.

Model estimates allow for a year-specific tide, and this is currently
estimated from voter turnout in the August statewide party primary. This
year, I adjust this prediction among the 6% of voters slated for
cancellation. I estimate that about only 22-26% of those could still
vote in Michigan across 2018 and 2020.

## Model specifics

Define
![y\_{ijt}](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;y_%7Bijt%7D
"y_{ijt}") as the set of QVF observations of whether a registered voter
![i](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;i
"i") in precinct
![j](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;j
"j") turned out to vote in election
![t](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;t
"t"). Voter probabilities of turning out to vote are then modeled as
follows:   
![&#10;\\Pr\[y\_{ijt} = 1\] = \\Lambda( x\_{it}'\\beta + z\_{t}'\\delta
+ Aug\_t( \\gamma\_{1} + \\zeta\_{1j} + \\nu\_{1i} ) +
Competition\_t(\\gamma\_{2} + \\nu\_{2i}) + \\zeta\_{0j} + \\nu\_{0i}
)&#10;](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;%0A%5CPr%5By_%7Bijt%7D%20%3D%201%5D%20%3D%20%5CLambda%28%20x_%7Bit%7D%27%5Cbeta%20%2B%20z_%7Bt%7D%27%5Cdelta%20%2B%20Aug_t%28%20%5Cgamma_%7B1%7D%20%2B%20%5Czeta_%7B1j%7D%20%2B%20%5Cnu_%7B1i%7D%20%29%20%2B%20Competition_t%28%5Cgamma_%7B2%7D%20%2B%20%5Cnu_%7B2i%7D%29%20%2B%20%5Czeta_%7B0j%7D%20%2B%20%5Cnu_%7B0i%7D%20%29%0A
"
\\Pr[y_{ijt} = 1] = \\Lambda( x_{it}'\\beta + z_{t}'\\delta + Aug_t( \\gamma_{1} + \\zeta_{1j} + \\nu_{1i} ) + Competition_t(\\gamma_{2} + \\nu_{2i}) + \\zeta_{0j} + \\nu_{0i} )
")  
where

  - ![x\_{it}](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;x_%7Bit%7D
    "x_{it}") are of set time-varying individual level predictors:
    logarithm of age;the interaction of log age with August primaries;
    the interaction of log age with the midterm cycle; a logarithm of
    month since registration; and dummies for an initial observation,
    whether a voter had recently registered in that quarter, and whether
    that recent regisration was during a presidential cycle.
  - ![z\_{t}](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;z_%7Bt%7D
    "z_{t}") are a set of election-specific indicators: whether the
    contest is a presidential primary; whether that election is during a
    midterm cycle; and the year of the contest.
  - ![Aug\_t](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;Aug_t
    "Aug_t") is an indicator of whether the election is an August
    primary. The relationship between this variable and a voter turnout
    is allowed to vary at the precinct level and at the individual-level
    (![\\zeta\_{1j} +
    \\nu\_{1i}](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;%5Czeta_%7B1j%7D%20%2B%20%5Cnu_%7B1i%7D
    "\\zeta_{1j} + \\nu_{1i}")). In other words, August turnout is
    assumed typically higher or lower in some precincts and among some
    individuals for factors beyond what is in the model. This
    specification essentially allows some individuals and precincts to
    be high turnout precincts in November but not in August (and
    vice-versa).
  - ![Competition\_t](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;Competition_t
    "Competition_t") is a (logged) measure counting statewide and U.S.
    House contests with major party competition or, for August, with
    primary competition. This effect is allowed to vary by individual
    (![\\nu\_{2i}](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;%5Cnu_%7B2i%7D
    "\\nu_{2i}")). In short, it allows that some people show up to vote
    regardless of the amount of competition on the ballot. Whereas other
    people’s likelihood of voting is responsive to the amount of
    competition on the ballot.
  - ![\\zeta\_{0j}](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;%5Czeta_%7B0j%7D
    "\\zeta_{0j}") is a precinct-specific, time constant parameter that
    captures that precinct’s unexplained tendency to have higher or
    lower registered voter turnout rates.
  - ![\\nu\_{0i}](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;%5Cnu_%7B0i%7D
    "\\nu_{0i}") is an individual-specific, time constant parameter that
    captures that individual’s unexplained tendency to have higher or
    lower voter turnout rates.

## Subsampling and prediction generation

Multilevel logit model estimates were performed over 10 separate
subsamples of 100 randomly sampled precincts (weighted by population)
with up to 50 randomly sampled voters within each precinct. Since the
average voter has a voter history spanning back ten past contests. There
are about 50,000 observations for each of the 10 estimates. These
parameters are then averaged and used to generate Empirical Bayes
estimates of the precinct and individual specific error components
(![\\zeta,
\\nu](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;%5Czeta%2C%20%5Cnu
"\\zeta, \\nu")), using 5-7 integration points. These Empirical Bayes
estimates are then combined with the model’s fixed portion to generate a
prediction of each voter’s probability of turning out to vote in
November.
